﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel;
using Windows.UI.Xaml.Media.Imaging;
using EagleAndTails.Command;
using EagleAndTails.Helpers;

namespace EagleAndTails.ViewModel
{
	class MainPageModel: BaseViewModel
	{
		private string _buttonTitle = ConstValues.ButtonInfo.Title;
		private string _coinTossResult = ConstValues.CoinInfo.DefaultTitle;
		private string _pathToImageSideCoin = ConstValues.PathEagle;
		private string _coinEagleTitle = ConstValues.CoinInfo.EagleTitle;
		private string _coinTailTitle = ConstValues.CoinInfo.TailTitle;
		private int _countEagles = 0;
		private int _countTails = 0;

		public ICommand TossCoinCommand { protected set; get; }

		public string ButtonTitle
		{
			get
			{
				return _buttonTitle;
			}
			set
			{
				if (value != _buttonTitle)
				{
					_buttonTitle = value;
					OnPropertyChanged();
				}
			}
		}
		public string CoinTossResult
		{
			get
			{
				return _coinTossResult;
			}
			set
			{
				if (value != _coinTossResult)
				{
					_coinTossResult = value;
					OnPropertyChanged();
				}
			}
		}
		public string PathToImageSideCoin
		{
			get
			{
				return _pathToImageSideCoin;
			}
			set
			{
				if (value != _pathToImageSideCoin)
				{
					_pathToImageSideCoin = value;
					OnPropertyChanged();
				}
			}
		}
		public string CoinEagleTitle
		{
			get
			{
				return _coinEagleTitle;
			}
			set
			{
				if (value != _coinEagleTitle)
				{
					_coinEagleTitle = value;
					OnPropertyChanged();
				}
			}
		}
		public string CoinTailTitle
		{
			get
			{
				return _coinTailTitle;
			}
			set
			{
				if (value != _coinTailTitle)
				{
					_coinTailTitle = value;
					OnPropertyChanged();
				}
			}
		}
		public int CountEagles
		{
			get
			{
				return _countEagles;
			}
			set
			{
				if (value != _countEagles)
				{
					_countEagles = value;
					OnPropertyChanged();
				}
			}
		}
		public int CountTails
		{
			get
			{
				return _countTails;
			}
			set
			{
				if (value != _countTails)
				{
					_countTails = value;
					OnPropertyChanged();
				}
			}
		}

		public MainPageModel()
		{
			TossCoinCommand = new RealyCommand(TossCoin);
		}

		private Random rnd = new Random();
		public void TossCoin(object obj)
		{
			int newSide = rnd.Next(0, 2);

			if (newSide == (int)CoinSides.Eagle)
			{
				CountEagles++;
				PathToImageSideCoin = ConstValues.PathEagle;
				CoinTossResult = ConstValues.CoinInfo.EagleTitle;
			}
			else
			{
				CountTails++;
				PathToImageSideCoin = ConstValues.PathTail;
				CoinTossResult = ConstValues.CoinInfo.TailTitle;
			}
		}

	}
}
