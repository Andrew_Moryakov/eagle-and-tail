﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EagleAndTails.Helpers
{
	public interface IButonInfo
	{
		string Title{ get; } 
	}
	public interface ICoinInfo
	{
		string EagleTitle{ get; }
		string TailTitle { get; }
		string DefaultTitle{ get; }
	}

	internal class ButtonInfoRu: IButonInfo
	{
		public string Title => "Подбросить";
	}

	internal class CoinInfoRu : ICoinInfo
	{
		public string EagleTitle => "Орел";
		public string TailTitle => "Решка";
		public string DefaultTitle => "Орел или решка";
	}

	internal class ButtonInfoInvariant : IButonInfo
	{
		public string Title => "Coin toss";
	}

	internal class CoinInfoInvariant : ICoinInfo
	{
		public string EagleTitle => "Eagle";
		public string TailTitle => "Tail";
		public string DefaultTitle => "Eagle or Tail";
	}

	public class ConstValues
	{
		static ConstValues()
		{
			bool isRuCulture =false;//= CultureInfo.CurrentCulture.Name.Equals("ru");
			if (isRuCulture)
			{
				_coinInfo = new CoinInfoRu();
				_buttonInfo = new ButtonInfoRu();
			}
			else
			{
				_coinInfo = new CoinInfoInvariant();
				_buttonInfo = new ButtonInfoInvariant();
			}
		}

		private static readonly ICoinInfo _coinInfo;
		private static readonly IButonInfo _buttonInfo;

		public static ICoinInfo CoinInfo{ get { return _coinInfo; } }
		public static IButonInfo ButtonInfo{ get { return _buttonInfo; } }

		public static string PathEagle => @"ms-appx:///Content/Pictures/Eagle.jpg";
		public static string PathTail => @"ms-appx:///Content/Pictures/Tail.jpg";
	}
}