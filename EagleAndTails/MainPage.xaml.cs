﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media.Media3D;
using Windows.UI.Xaml.Navigation;
using EagleAndTails.Helpers;
using Microsoft.AdMediator.Core.Models;

// Документацию по шаблону элемента "Пустая страница" см. по адресу http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace EagleAndTails
{
	/// <summary>
	/// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
	/// </summary>
	public sealed partial class MainPage: Page
	{
		public MainPage()
		{
			this.InitializeComponent();

			AdMediator_DB91B9.AdSdkError += AdMediator_Bottom_AdError;
			AdMediator_DB91B9.AdMediatorFilled += AdMediator_Bottom_AdFilled;
			AdMediator_DB91B9.AdMediatorError += AdMediator_Bottom_AdMediatorError;
			AdMediator_DB91B9.AdSdkEvent += AdMediator_Bottom_AdSdkEvent;

		}
		void AdMediator_Bottom_AdSdkEvent(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
		{
			Debug.WriteLine("AdSdk event {0} by {1}", e.EventName, e.Name);
		}

		void AdMediator_Bottom_AdMediatorError(object sender, Microsoft.AdMediator.Core.Events.AdMediatorFailedEventArgs e)
		{
			Debug.WriteLine("AdMediatorError:" + e.Error + " " + e.ErrorCode);
			// if (e.ErrorCode == AdMediatorErrorCode.NoAdAvailable)
			// AdMediator will not show an ad for this mediation cycle
		}

		void AdMediator_Bottom_AdFilled(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
		{
			Debug.WriteLine("AdFilled:" + e.Name);
		}

		void AdMediator_Bottom_AdError(object sender, Microsoft.AdMediator.Core.Events.AdFailedEventArgs e)
		{
			Debug.WriteLine("AdSdkError by {0} ErrorCode: {1} ErrorDescription: {2} Error: {3}", e.Name, e.ErrorCode, e.ErrorDescription, e.Error);
		}
		private void textBlock_SelectionChanged(object sender, RoutedEventArgs e)
		{

		}
	}
}